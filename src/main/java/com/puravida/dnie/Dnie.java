package com.puravida.dnie;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import sun.security.pkcs11.SunPKCS11;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Stream;

public class Dnie {

    public static void main(String ...args){
        if( args.length < 3 ){
            System.out.println("Dime el pdf a firmar, donde dejarlo y la password del DNIe");
            return;
        }
        File file = new File(args[0]);
        if( file.exists() == false ){
            System.out.println("Dime un pdf que exista a firmar al menos");
            return;
        }

        Dnie dnie = new Dnie(file, args[2]);
        dnie.init();
        if( dnie.identify() )
            dnie.sign(new File(args[1]));
    }

    String passwordDnie;
    File pdfOriginal;
    SunPKCS11 provider;

    Dnie(File pdf, String password){
        this.pdfOriginal = pdf;
        this.passwordDnie = password;
    }

    public void init(){
        File fso = new File("/usr/lib/opensc-pkcs11.so");
        if( !fso.exists() ) {
            fso = new File("/usr/lib/libpkcs11-dnie.so");
            if (!fso.exists())
                throw new RuntimeException("DNIe.so required");
        }
        String config = String.join("\n",
                "name=DNIe",
                "library="+fso.getAbsolutePath()
        );

        provider = new sun.security.pkcs11.SunPKCS11( new ByteArrayInputStream(config.getBytes()) );

        Security.addProvider(provider);
    }

    PrivateKey privateKey;
    X509Certificate x509Certificate;

    public boolean identify(){
        try {
            CallbackHandler callbackHandler = new CallbackHandler() {
                @Override
                public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
                    for (Callback callback : callbacks) {
                        if (callback instanceof PasswordCallback) {
                            PasswordCallback passwordCallback = (PasswordCallback) callback;
                            passwordCallback.setPassword(passwordDnie.toCharArray());
                        }
                    }
                }
            };

            provider.login(new Subject(), callbackHandler);

            KeyStore keyStore = KeyStore.getInstance("PKCS11", provider);

            keyStore.load(null, null);

            privateKey = (PrivateKey) keyStore.getKey("CertAutenticacion", null);
            x509Certificate = (X509Certificate)keyStore.getCertificate("CertAutenticacion");

            final org.bouncycastle.asn1.x509.Certificate x509CertificateBouncyCastle =
                    org.bouncycastle.asn1.x509.Certificate.getInstance(ASN1Primitive.fromByteArray(x509Certificate.getEncoded()));


            System.out.println(x509CertificateBouncyCastle.getSubject().getRDNs(BCStyle.GIVENNAME)[0].getFirst().getValue());
            System.out.println(x509CertificateBouncyCastle.getSubject().getRDNs(BCStyle.SURNAME)[0].getFirst().getValue());
            System.out.println(x509CertificateBouncyCastle.getSubject().getRDNs(BCStyle.SERIALNUMBER)[0].getFirst().getValue());

            return true;

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean sign(final File fDestination ){
        try{
            final InputStream fSource = new FileInputStream(pdfOriginal);

            fDestination.mkdirs();
            Files.copy(fSource, Paths.get(fDestination.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);

            final PDDocument doc = PDDocument.load(fDestination);
            final PDSignature signature = new PDSignature();

            signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
            signature.setSubFilter(PDSignature.SUBFILTER_ADBE_PKCS7_DETACHED);
            signature.setSignDate(Calendar.getInstance());

            doc.addSignature(signature, new SignatureInterface() {
                @Override
                public byte[] sign(InputStream content) throws IOException {
                    try {
                        List<Certificate> certList = Arrays.asList(Dnie.this.x509Certificate);

                        Store certs = new JcaCertStore(certList);

                        CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
                        org.bouncycastle.asn1.x509.Certificate cert =
                                org.bouncycastle.asn1.x509.Certificate.getInstance(
                                        ASN1Primitive.fromByteArray(Dnie.this.x509Certificate.getEncoded()));

                        ContentSigner sha1Signer = new JcaContentSignerBuilder("SHA256WithRSA")
                                .build(Dnie.this.privateKey);

                        gen.addSignerInfoGenerator(
                                new JcaSignerInfoGeneratorBuilder(
                                        new JcaDigestCalculatorProviderBuilder().build()).
                                        build(sha1Signer, new X509CertificateHolder(cert)));
                        gen.addCertificates(certs);

                        CMSProcessableInputStream msg = new CMSProcessableInputStream(content);
                        CMSSignedData signedData = gen.generate(msg, false);

                        return signedData.getEncoded();
                    }catch (Exception e){
                        throw new IOException(e.getMessage());
                    }
                }
            });

            FileOutputStream fos = new FileOutputStream(fDestination);
            doc.saveIncremental(fos);

            return true;

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
