import com.puravida.dnie.Dnie
import spock.lang.Specification

class DnieSpec extends Specification{

    void "simple test"(){

        given:
        File pdf = new File("test.pdf")

        and:
        Dnie dnie = new Dnie(pdf, "la pwd del dni")

        when:
        dnie.init()

        and:
        dnie.identify()

        then:
        dnie.sign( new File("build/test.pdf"))

    }

}